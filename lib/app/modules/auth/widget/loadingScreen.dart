import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:topik_chat/app/modules/auth/controllers/auth_controller.dart';
import 'package:topik_chat/app/modules/auth/service/auth_service.dart';
import 'package:topik_chat/app/modules/auth/views/login.dart';
import 'package:topik_chat/shared/http_request.dart';
import 'package:topik_chat/shared/theme.dart';

class LoadingScreen extends StatefulWidget {
  final VoidCallback? toNext;
  const LoadingScreen({this.toNext, super.key});

  @override
  State<LoadingScreen> createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  final GetStorage storage = GetStorage();

  @override
  void initState() {
    super.initState();
    var user = storage.read("user");
    if (user != null) {
      HttpRequest.updateToken(user['token']);
      Timer(Duration(seconds: 1), AuthService.beranda);
    } else {
      Timer(Duration(seconds: 1), widget.toNext!);
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      body: Center(
        child: Container(
          width: 200,
          height: 200,
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/logo-topik.png"),
            ),
          ),
        ),
      ),
    );
  }
}
