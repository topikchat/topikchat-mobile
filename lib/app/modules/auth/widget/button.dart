import 'package:flutter/material.dart';
import 'package:topik_chat/shared/theme.dart';

class CustomeFilledButton extends StatelessWidget {
  final String title;
  final double width;
  final double height;
  final VoidCallback? onPressed;
  final Icon? icon;
  final bool isRounded;
  final Color? color;

  const CustomeFilledButton(
      {super.key,
      required this.title,
      this.width = double.infinity,
      this.height = 50,
      this.onPressed,
      this.icon,
      this.isRounded = true,
      this.color});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: 50,
      child: TextButton(
        onPressed: onPressed,
        style: TextButton.styleFrom(
          backgroundColor: color ?? buttonLogin,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(isRounded ? 50 : 8),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (icon != null) icon!,
            if (icon != null) SizedBox(width: 8),
            Text(
              title,
              style: TextStyle(color: whiteColor, fontWeight: semibold),
            ),
          ],
        ),
      ),
    );
  }
}

class CustomeTextButtons extends StatelessWidget {
  final String title;
  final double width;
  final double height;
  final VoidCallback? onPressed;

  const CustomeTextButtons(
      {super.key,
      required this.title,
      this.width = double.infinity,
      this.height = 24,
      this.onPressed});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: TextButton(
        onPressed: onPressed,
        style: TextButton.styleFrom(
          padding: EdgeInsets.zero,
        ),
        child: Text(
          title,
          style: greyTextStyle.copyWith(
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}
