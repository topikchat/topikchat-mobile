import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:topik_chat/app/modules/auth/service/auth_service.dart';
import 'package:topik_chat/app/modules/auth/views/register.dart';
import 'package:topik_chat/app/modules/beranda/service/berandaService.dart';
import 'package:topik_chat/shared/theme.dart';
import 'package:topik_chat/app/modules/auth/widget/button.dart';
import 'package:topik_chat/app/modules/auth/widget/forms.dart';

import '../controllers/auth_controller.dart';

class LoginScreen extends GetView<AuthController> {
  const LoginScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: lightBackgroundColor,
      body: ListView(
        padding: EdgeInsets.symmetric(
          horizontal: 24,
        ),
        children: [
          SizedBox(
            height: 70,
          ),
          Center(
            child: Image.asset(
              "assets/logo-topik.png",
              width: 156.w,
              height: 156.h,
            ),
          ),
          Center(
            child: Text(
              "Login",
              style: TextStyle(fontSize: 30.sp, fontWeight: medium),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Container(
            padding: EdgeInsets.all(22),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20), color: whiteColor),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // NOTE EMAIL INPUT
                CustomeFormFilled(
                  title: 'Email / Username',
                  controller: controller.usernameController,
                ),
                SizedBox(
                  height: 16,
                ),
                // NOTE PASSWORD INPUT
                CustomeFormFilled(
                  title: 'Kata Sandi',
                  controller: controller.passwordController,
                  obscureText: true,
                ),
                SizedBox(
                  height: 16,
                ),

                CustomeFilledButton(
                  title: 'Login',
                  onPressed: () {
                    AuthService.loginProses(
                      email: controller.usernameController.text,
                      username: controller.usernameController.text,
                      password: controller.passwordController.text,
                    );
                  },
                ),
              ],
            ),
          ),
          SizedBox(
            height: 15,
          ),
          CustomeTextButtons(
            title: 'Register',
            onPressed: () {
              AuthService.loadingScreen(toNext: () {
                AuthService.register();
              });
            },
          ),
        ],
      ),
    );
  }
}
