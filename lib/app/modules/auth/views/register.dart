import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:topik_chat/app/modules/auth/service/auth_service.dart';
import 'package:topik_chat/shared/theme.dart';
import 'package:topik_chat/app/modules/auth/widget/button.dart';
import 'package:topik_chat/app/modules/auth/widget/forms.dart';

import '../controllers/auth_controller.dart';

class RegisterScreen extends GetView<AuthController> {
  const RegisterScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: lightBackgroundColor,
      body: ListView(
        padding: EdgeInsets.symmetric(
          horizontal: 24,
        ),
        children: [
          SizedBox(
            height: 25,
          ),
          Center(
            child: Image.asset(
              "assets/logo-topik.png",
              width: 156.w,
              height: 156.h,
            ),
          ),
          Center(
            child: Text(
              "Register",
              style: TextStyle(fontSize: 30.sp, fontWeight: medium),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Container(
            padding: EdgeInsets.all(22),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: whiteColor,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // NOTE EMAIL INPUT
                CustomeFormFilled(
                  title: 'Nama',
                  controller: controller.namaController,
                ),
                SizedBox(
                  height: 16.h,
                ),
                // NOTE PASSWORD INPUT
                CustomeFormFilled(
                  title: 'Username',
                  controller: controller.usernameController,
                ),
                SizedBox(
                  height: 8.h,
                ),
                CustomeFormFilled(
                  title: 'Email',
                  controller: controller.emailController,
                ),
                SizedBox(
                  height: 8.h,
                ),
                CustomeFormFilled(
                  title: 'Kata Sandi',
                  controller: controller.passwordController,
                  obscureText: true,
                ),
                SizedBox(
                  height: 8.h,
                ),
                SizedBox(
                  height: 30.h,
                ),
                CustomeFilledButton(
                  title: 'Buat Akun',
                  onPressed: () {
                    AuthService.registerProses(
                      name: controller.namaController.text,
                      email: controller.emailController.text,
                      username: controller.usernameController.text,
                      password: controller.passwordController.text,
                    );
                  },
                )
              ],
            ),
          ),
          SizedBox(
            height: 15.h,
          ),
          CustomeTextButtons(
            title: 'Login',
            onPressed: () {
              AuthService.loadingScreen(toNext: () {
                AuthService.login();
              });
            },
          ),
        ],
      ),
    );
  }
}
