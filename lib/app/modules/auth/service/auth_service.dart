import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:topik_chat/shared/dialogs.dart';
import 'package:topik_chat/shared/http_request.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:topik_chat/app/modules/auth/controllers/auth_controller.dart';
import 'package:topik_chat/app/modules/auth/widget/loadingScreen.dart';
import 'package:topik_chat/app/modules/beranda/views/beranda.dart';
import 'package:topik_chat/app/modules/tambah_topik/views/tambah_topik_view.dart';
import 'package:get_storage/get_storage.dart';

abstract class AuthService {
  static AuthController authController = Get.find<AuthController>();
  static GetStorage storage = GetStorage();

  static void login() {
    Get.offAllNamed("/login");
  }

  static void register() {
    Get.offAllNamed("/register");
  }

  static void beranda() {
    Get.offAllNamed("/beranda");
  }

  static void editProfile() {
    Get.toNamed("/editProfile");
  }

  static void loadingScreen({VoidCallback? toNext}) {
    Get.offAll(() => LoadingScreen(
          toNext: toNext,
        ));
  }

  static void registerProses({
    String name = '',
    String username = '',
    String email = '',
    String password = '',
  }) async {
    if (name == '' || username == '' || email == '' || password == '') {
      dialogError('Semua kolom harus di isi');
    } else {
      Dio dio = HttpRequest.dio;
      var response = await dio.post(
        "/v1/register",
        data: jsonEncode(
          {
            "name": name,
            "email": email,
            "username": username,
            "password": password,
          },
        ),
      );
      if (response.statusCode == 200) {
        Get.defaultDialog(
          title: "Register Berhasil",
          middleText: "Memindahkan Anda ke Halaman Login...",
        );
        await Future.delayed(const Duration(seconds: 1));
        loadingScreen(
          toNext: () {
            AuthService.login();
          },
        );
      } else {
        dialogError(response.data["message"]);
      }
    }
  }

  static void logout({langsung = false}) async {
    if (!langsung) {
      Dio dio = HttpRequest.dio;
      await dio.post("/v1/logout");
    }

    storage.remove("user");
    Get.offAllNamed("/login");
  }

  static Map<String, dynamic>? getUser() {
    var user = storage.read("user");
    if (user != null) {
      return user;
    }
    dialogError("User tidak ditemukan! Silahkan login kembali");
    return null;
  }

  static void loginProses({
    String username = '',
    String email = '',
    String password = '',
  }) async {
    print("A");
    if (email == "" || username == "" || password == "") {
      dialogError("Email/Username atau Password Salah!!");
    } else {
      Dio dio = HttpRequest.dio;
      var response = await dio.post(
        "/v1/login",
        data: jsonEncode(
          {
            "email": email,
            "username": username,
            "password": password,
          },
        ),
      );
      if (response.statusCode == 200) {
        var data = response.data['data'];
        HttpRequest.updateToken(data['token']);
        storage.write("user", data);
        await Future.delayed(Duration(seconds: 1));
        loadingScreen(toNext: () {
          AuthService.beranda();
        });
      } else {
        dialogError(response.data["message"]);
      }
    }
  }
}
