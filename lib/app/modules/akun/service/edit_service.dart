import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:topik_chat/app/modules/auth/controllers/auth_controller.dart';
import 'package:topik_chat/app/modules/beranda/views/beranda.dart';
import 'package:topik_chat/app/modules/tambah_topik/views/tambah_topik_view.dart';
import 'package:topik_chat/shared/http_request.dart';

abstract class EditService {
  static AuthController authController = Get.find<AuthController>();

  static void editProfile() {
    Get.toNamed("/editProfile");
  }

  static Future<Map<String, dynamic>> editProses(
      String nama, String email, String username) async {
    Dio dio = HttpRequest.dio;
    var response = await dio.put(
      "/v1/edit-profile",
      data: jsonEncode(
        {
          "name": nama,
          "email": email,
          "username": username,
        },
      ),
    );
    var data = response.data;
    if (response.statusCode == 200) {
      Get.defaultDialog(
        title: "Berhasil Update",
        middleText: "Memindahkan Anda ke Halaman Beranda..... ",
      );
      return data;
    }
    throw Exception(data["message"]);
  }
}
