import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:topik_chat/app/modules/akun/service/edit_service.dart';
import 'package:topik_chat/app/modules/auth/service/auth_service.dart';
import 'package:topik_chat/shared/dialogs.dart';

class AkunController extends GetxController {
  late final TextEditingController name;
  late final TextEditingController email;
  late final TextEditingController username;
  late var user;
  GetStorage storage = GetStorage();

  @override
  void onInit() {
    name = TextEditingController();
    email = TextEditingController();
    username = TextEditingController();
    user = storage.read("user");

    name.text = user['name'];
    email.text = user['email'];
    username.text = user['username'];

    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    name.dispose();
    email.dispose();
    username.dispose();
    super.onClose();
  }

  Future<void> editProfileProses() async {
    if (email.text == "" || name.text == "" || username.text == "") {
      dialogError("Data Wajib diisi!!");
    }
    try {
      var data =
          await EditService.editProses(name.text, email.text, username.text);
      var userBaru = data["data"];
      user = AuthService.getUser();
      userBaru["token"] = user["token"];
      storage.write("user", userBaru);

      await Future.delayed(Duration(seconds: 1));
      AuthService.loadingScreen();
    } catch (e) {
      dialogError(e.toString());
    }
  }
}
