import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:topik_chat/app/modules/akun/controllers/akun_controller.dart';
import 'package:topik_chat/app/modules/akun/service/edit_service.dart';
import 'package:topik_chat/app/modules/auth/service/auth_service.dart';
import 'package:topik_chat/app/modules/auth/widget/button.dart';
import 'package:topik_chat/app/modules/auth/widget/forms.dart';
import 'package:topik_chat/shared/theme.dart';

class EditProfile extends StatefulWidget {
  EditProfile({super.key});

  @override
  State<EditProfile> createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  final AkunController akunController = Get.find<AkunController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: blackColor),
        backgroundColor: lightBackgroundColor,
        title: Text(
          'Edit Profile',
          style:
              TextStyle(fontSize: 26, fontWeight: medium, color: buttonLogin),
        ),
      ),
      backgroundColor: lightBackgroundColor,
      body: ListView(
        padding: EdgeInsets.symmetric(
          horizontal: 24,
        ),
        children: [
          Container(
            width: 155,
            height: 80,
          ),
          Container(
            margin: EdgeInsets.only(top: 90),
            padding: EdgeInsets.all(22),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20), color: whiteColor),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // NOTE EMAIL INPUT

                // NOTE PASSWORD INPUT
                CustomeFormFilled(
                  title: 'Nama',
                  controller: akunController.name,
                ),
                SizedBox(
                  height: 8,
                ),
                CustomeFormFilled(
                  title: 'Email',
                  controller: akunController.email,
                ),
                SizedBox(
                  height: 8,
                ),
                CustomeFormFilled(
                  title: 'Username',
                  controller: akunController.username,
                ),
                SizedBox(
                  height: 8,
                ),
                SizedBox(
                  height: 30,
                ),
                CustomeFilledButton(
                  title: 'Submit',
                  onPressed: () {
                    akunController.editProfileProses();
                  },
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
