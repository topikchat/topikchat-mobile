import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:topik_chat/app/modules/auth/controllers/auth_controller.dart';
import 'package:topik_chat/app/modules/chat/controllers/chat_controller.dart';
import 'package:topik_chat/app/modules/chat/views/chat.dart';
import 'package:topik_chat/app/routes/app_pages.dart';
import 'package:topik_chat/shared/http_request.dart';

class ChatService {
  Future<Map<String, dynamic>> getTopikData(int id) async {
    Dio dio = HttpRequest.dio;
    try {
      var response = await dio.get("/v1/group-chat/$id");
      var data = response.data;
      if (response.statusCode == 200) {
        return data['data'];
      }
      throw data['message'];
    } on DioError catch (e) {
      throw e.response!.data['message'];
    }
  }

  Future<List> getRequestData(int id) async {
    Dio dio = HttpRequest.dio;
    try {
      var response = await dio.get("/v1/group-chat/$id/pendings");
      var data = response.data;
      if (response.statusCode == 200) {
        return data['data'];
      }
      throw data['message'];
    } on DioError catch (e) {
      throw e.response!.data['message'];
    }
  }

  Future<String> joinTopik(int groupId, int userId) async {
    Dio dio = HttpRequest.dio;
    try {
      var response = await dio.post(
        "/v1/chat-networks",
        data: jsonEncode({
          'group_id': groupId,
          'user_id': userId,
        }),
      );
      var data = response.data;
      if (response.statusCode == 201) {
        return data['message'];
      }
      throw data['message'];
    } on DioError catch (e) {
      throw e.response!.data['message'];
    }
  }

  Future<String> sendChat(int groupId, int userId, String message) async {
    Dio dio = HttpRequest.dio;
    try {
      var response = await dio.post(
        "/v1/chats",
        data: jsonEncode({
          'group_id': groupId,
          'user_id': userId,
          'message': message,
        }),
      );
      var data = response.data;
      if (response.statusCode == 201) {
        return data['message'];
      }
      throw data['message'];
    } on DioError catch (e) {
      throw e.response!.data['message'];
    }
  }

  Future<List> getChats(int groupId) async {
    Dio dio = HttpRequest.dio;
    try {
      var response = await dio.get(
        "/v1/chats?group_id=$groupId",
      );
      var data = response.data;
      if (response.statusCode == 200) {
        return data['data'];
      }
      throw data['message'];
    } on DioError catch (e) {
      throw e.response!.data['message'];
    }
  }

  Future<String> acceptUser(int id) async {
    Dio dio = HttpRequest.dio;
    try {
      var response = await dio.put(
        "/v1/chat-networks/$id",
        data: jsonEncode({
          'approved_at': DateTime.now().toString(),
        }),
      );
      var data = response.data;
      if (response.statusCode == 200) {
        return data['message'];
      }
      throw data['message'];
    } on DioError catch (e) {
      throw e.response!.data['message'];
    }
  }

  Future<String> editChat(dynamic chatId, String message) async {
    Dio dio = HttpRequest.dio;
    try {
      var response = await dio.put(
        "/v1/chats/$chatId",
        data: jsonEncode({
          'message': message,
        }),
      );
      var data = response.data;
      if (response.statusCode == 200) {
        return data['message'];
      }
      throw data['message'];
    } on DioError catch (e) {
      throw e.response!.data['message'];
    }
  }

  Future<String> deleteChat(dynamic id) async {
    Dio dio = HttpRequest.dio;
    try {
      var response = await dio.delete(
        "/v1/chats/$id",
      );

      var data = response.data;
      if (response.statusCode == 200) {
        return data['message'];
      }
      throw data['message'];
    } on DioError catch (e) {
      throw e.response!.data['message'];
    }
  }

  Future<void> deleteTopik(int id) async {
    Dio dio = HttpRequest.dio;
    try {
      var response = await dio.delete("/v1/group-chat/$id");
      var data = response.data;
      if (response.statusCode == 200) {
        return;
      }
      throw data['message'];
    } on DioError catch (e) {
      throw e.response!.data['message'];
    }
  }

  //   Future<String> keluarGroup(dynamic id) async {
  //   Dio dio = HttpRequest.dio;
  //   try {
  //     var response = await dio.delete(
  //       "/v1/chat-networks/$id",
  //     );

  //     var data = response.data;
  //     if (response.statusCode == 200) {
  //       return data['message'];
  //     }
  //     throw data['message'];
  //   } on DioError catch (e) {
  //     throw e.response!.data['message'];
  //   }
  // }
}
