import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:topik_chat/app/modules/auth/service/auth_service.dart';
import 'package:topik_chat/app/modules/chat/Service/chat_service.dart';
import 'package:topik_chat/app/modules/chat/views/chat.dart';
import 'package:topik_chat/app/routes/app_pages.dart';
import 'package:topik_chat/shared/dialogs.dart';
import 'package:topik_chat/shared/theme.dart';

class ChatController extends GetxController {
  RxList dataChat = [].obs;
  RxList dataRequest = [].obs;
  Rx<Map?> dataGroup = Rx<Map?>(null);
  var user;
  GetStorage storage = GetStorage();
  late final TextEditingController chatBox;
  late final TextEditingController editMessage;
  final ChatService chatService = ChatService();
  RxList selectedChats = [].obs;

  Future<void> getTopikData(int id) async {
    try {
      var data = await chatService.getTopikData(id);
      if (data['joined'] == true) {
        await getChatData(id);
      }
      dataGroup.value = data;
    } catch (e) {
      dialogError(e.toString());
    }
  }

  Future<void> getChatData(int groupId) async {
    try {
      var data = await chatService.getChats(groupId);
      dataChat.value = data;
    } catch (e) {
      dialogError(e.toString());
    }
  }

  Future<void> getRequestData(int id) async {
    try {
      var data = await chatService.getRequestData(id);
      dataRequest.value = data;
    } catch (e) {
      print("HERE request");

      dialogError(e.toString());
    }
  }

  Future<void> sendChat(int groupId, int userId, String message) async {
    try {
      if (message.isEmpty) {
        dialogError("Chat harus diisi!");
        return;
      }
      dataChat.add(
        {
          "id": 0,
          "user_id": user['id'].toString(),
          "group_id": dataGroup.value!['id'],
          "message": message,
          "media_link": null,
          "sent_at": DateTime.now(),
          "created_at": DateTime.now(),
          "updated_at": DateTime.now(),
          "user": user,
        },
      );
      chatBox.text = "";
      editMessage.text = '';
      await chatService.sendChat(groupId, userId, message);
    } catch (e) {
      getChatData(groupId);
      dialogError(e.toString());
    }
  }

  Future<void> acceptUser(int id, int groupId) async {
    try {
      await chatService.acceptUser(id);
      Get.back();
      Future.delayed(const Duration(seconds: 1), () async {
        await getTopikData(groupId);
        await getChatData(groupId);
      });
    } catch (e) {
      dialogError(e.toString());
    }
  }

  Future<void> editChat() async {
    try {
      var message = editMessage.text;
      var chatId = selectedChats[0]['id'];
      print("$message, chat: $chatId");
      Get.back();
      if (message.isEmpty) {
        dialogError("Chat harus diisi!");
        return;
      }
      dialogLoading();
      await chatService.editChat(chatId, message);
      Get.back();
      Get.defaultDialog(
        title: "TopikChat",
        middleText: "Berhasil merubah chat",
      );
    } catch (e) {
      dialogError(e.toString());
    }
    selectedChats.value = [];
  }

  Future<void> deleteChat(dynamic id, dynamic groupId) async {
    try {
      String message = await chatService.deleteChat(id);
    } catch (e) {
      Get.back();
      dialogError(e.toString());
    }
  }

  // Future<void> keluarGroup(
  //   dynamic id,
  // ) async {
  //   try {
  //     await chatService.keluarGroup(id);
  //   } catch (e) {
  //     Get.back();
  //     dialogError(e.toString());
  //   }
  // }

  Future<void> deleteChats() async {
    dialogLoading();
    if (selectedChats.length == 0) {
      Get.back();
      dialogError("Gagal Menghapus Chats");
      return;
    }
    for (var chat in selectedChats) {
      await deleteChat(chat['id'], chat['group_id']);
    }
    Get.back();
    Get.defaultDialog(
      title: "TopikChat",
      middleText: "Berhasil menghapus ${selectedChats.length} chat-mu",
    );

    selectedChats.value = [];
  }

  Future<void> joinTopik(int groupId, int userId) async {
    try {
      dialogLoading();
      String message = await chatService.joinTopik(groupId, userId);
      Get.back();
      Get.defaultDialog(
        title: "TopikChat",
        middleText: message,
      );
      Future.delayed(const Duration(seconds: 1), () async {
        Get.back();
        await getTopikData(groupId);
        await getChatData(groupId);
      });
    } catch (e) {
      Get.back();
      dialogError(e.toString());
    }
  }

  Future<void> deleteTopik(int id) async {
    try {
      await chatService.deleteTopik(id);
      Get.back();
      Get.defaultDialog(
        title: "TopikChat",
        middleText: "Berhasil menghapus topik.",
      );
      Future.delayed(
        const Duration(seconds: 1),
        () => Get.offAllNamed(Routes.BERANDA),
      );
    } catch (e) {
      dialogError(e.toString());
    }
  }

  void showEditDialog(onTap, controller) {
    print(selectedChats.length);
    if (selectedChats.length > 1) {
      dialogError("anda hanya dapat memilih satu chat");
      return;
    }
    editMessage.text = selectedChats[0]['message'];
    Get.defaultDialog(
        title: "edit text",
        content: SendButton(onTap: onTap, controller: controller));
  }

  @override
  void onInit() {
    // user = AuthService.getUser();
    user = storage.read("user");
    chatBox = TextEditingController();
    editMessage = TextEditingController();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
