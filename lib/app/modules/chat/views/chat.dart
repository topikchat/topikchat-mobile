import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:topik_chat/app/modules/auth/service/auth_service.dart';
import 'package:topik_chat/app/modules/auth/views/login.dart';
import 'package:topik_chat/app/modules/auth/widget/button.dart';
import 'package:topik_chat/app/modules/beranda/widget/ChatBar.dart';
import 'package:topik_chat/app/modules/beranda/widget/sidebar.dart';
import 'package:topik_chat/app/modules/chat/Service/chat_service.dart';
import 'package:topik_chat/app/modules/chat/widget/chat.dart';
import 'package:topik_chat/app/modules/chat/widget/myChat.dart';
import 'package:topik_chat/app/modules/tambah_topik/service/topikService.dart';
import 'package:topik_chat/app/modules/tambah_topik/views/tambah_topik_view.dart';
import 'package:topik_chat/shared/dialogs.dart';
import 'package:topik_chat/shared/theme.dart';
import "package:firebase_messaging/firebase_messaging.dart";

import '../controllers/chat_controller.dart';

class BuildChat extends StatefulWidget {
  const BuildChat({super.key});

  @override
  State<BuildChat> createState() => _BuildChatState();
}

class _BuildChatState extends State<BuildChat> {
  final GlobalKey<ScaffoldState> _drawerKey = GlobalKey();
  final ChatController chatController = Get.find<ChatController>();
  Map<String, dynamic> arguments = {};
  late int id;
  final GetStorage storage = GetStorage();

  var editMode;

  @override
  void initState() {
    arguments = Get.arguments;
    id = arguments['id'];
    refreshData();
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      final notification = message.notification;
      final data = message.data;

      if (notification != null) {}
      if (data.containsKey('group_id')) {
        chatController.getChatData(int.parse(data['group_id']));
      }
    });

    FirebaseMessaging.instance.subscribeToTopic("chat");

    super.initState();
  }

  Future<void> refreshData() async {
    await chatController.getTopikData(id);
    if (chatController.dataGroup.value?['joined'] ?? false) {
      await chatController.getChatData(id);
    }
  }

  void selectChat(topik) {
    if (chatController.selectedChats.contains(topik)) {
      chatController.selectedChats.remove(topik);
    } else {
      chatController.selectedChats.add(topik);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Obx(() {
      print(chatController.dataChat);
      bool? dataExist = chatController.dataGroup.value != null;
      bool? isJoined = false;
      bool? isOwner = false;
      bool? isRequesting = false;

      if (dataExist && chatController.user != null) {
        isJoined = chatController.dataGroup.value!['joined'];
        isOwner = chatController.dataGroup.value!['user_id'].toString() ==
            chatController.user['id'].toString();
        isRequesting = chatController.dataGroup.value!['is_requesting'];
      }
      return RefreshIndicator(
        onRefresh: refreshData,
        child: Column(
          children: [
            CustomAppBar(
              chatController: chatController,
            ),
            Expanded(
              child: Container(
                height: double.infinity,
                child: chatController.dataChat.isEmpty
                    ? Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(Icons.disabled_visible_outlined),
                            const SizedBox(height: 20),
                            Text(
                              isJoined != null && isJoined
                                  ? 'Belum ada chat'
                                  : "Anda belum bergabung",
                              style:
                                  TextStyle(fontSize: 22, fontWeight: regular),
                            ),
                            const SizedBox(height: 20),
                            if (isJoined != true && isRequesting != true)
                              GestureDetector(
                                onTap: () {
                                  chatController.joinTopik(
                                    chatController.dataGroup.value!['id'],
                                    chatController.user['id'],
                                  );
                                },
                                child: Chip(
                                  label: const Text("Gabung"),
                                  backgroundColor: buttonLogin,
                                ),
                              ),
                          ],
                        ),
                      )
                    : SingleChildScrollView(
                        reverse: true,
                        child: Column(
                          children: chatController.dataChat
                              .map((topik) => Column(
                                    children: [
                                      topik["user_id"].toString() ==
                                              chatController.user['id']
                                                  .toString()
                                          ? MyChat(
                                              id: topik["id"],
                                              name: topik["user"]["name"],
                                              message: topik["message"],
                                              setSelected: () =>
                                                  selectChat(topik),
                                              selected: chatController
                                                  .selectedChats.value
                                                  .contains(topik),
                                            )
                                          : Chat(
                                              id: topik["id"],
                                              name: topik["user"]["name"],
                                              message: topik["message"],
                                            ),
                                      const SizedBox(
                                        height: 10,
                                      )
                                    ],
                                  ))
                              .toList(),
                        ),
                      ),
              ),
            ),
            if (isJoined == true)
              SendButton(
                onTap: () => chatController.sendChat(
                  chatController.dataGroup.value!['id'],
                  chatController.user['id'],
                  chatController.chatBox.text,
                ),
                controller: chatController.chatBox,
              )
          ],
        ),
      );
    }));
  }
}

class REQUEST extends StatefulWidget {
  const REQUEST({Key? key, required this.groupId}) : super(key: key);

  final int groupId;
  @override
  State<REQUEST> createState() => _REQUESTState();
}

class _REQUESTState extends State<REQUEST> {
  final chatController = Get.find<ChatController>();

  @override
  void initState() {
    print("AA");
    chatController.getRequestData(widget.groupId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 10,
      ), // Increase horizontal padding
      title: const Center(
        child: Text("Daftar Request"),
      ),
      content: Obx(
        () => SingleChildScrollView(
          child: Column(
              children: chatController.dataRequest.isEmpty
                  ? [
                      const Text("Belum ada request masuk"),
                    ]
                  : chatController.dataRequest
                      .map(
                        (data) => createRequestItem(data),
                      )
                      .toList()),
        ),
      ),
    );
  }

  Row createRequestItem(Map data) {
    return Row(
      children: [
        const Icon(
          Icons.account_circle_outlined,
          size: 30,
        ),
        const SizedBox(width: 5),
        Flexible(
          child: Text(
            "${data['user']['name']} Ingin bergabung ke grup ini",
            style: const TextStyle(
              fontSize: 13,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        const SizedBox(width: 5),
        IconButton(
          color: Colors.green,
          onPressed: () {
            chatController.acceptUser(data['id'], data['group_id']);
          },
          icon: const Icon(
            Icons.check_circle,
            size: 24,
          ),
        ),
      ],
    );
  }
}

class CustomAppBar extends StatelessWidget {
  final ChatController chatController;

  CustomAppBar({
    required this.chatController,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Obx(
        () {
          bool? dataExist = chatController.dataGroup.value != null;
          bool? isJoined = false;
          bool? isOwner = false;
          bool? isRequesting = false;

          if (dataExist && chatController.user != null) {
            isJoined = chatController.dataGroup.value!['joined'];
            isOwner = chatController.dataGroup.value!['user_id'].toString() ==
                chatController.user['id'].toString();
            isRequesting = chatController.dataGroup.value!['is_requesting'];
            print(
                "dataExist: $dataExist, isJoined: $isJoined, isOwner: $isOwner, isRequesting: $isRequesting");
          }

          return Material(
            elevation: 1.2,
            child: Padding(
              padding: EdgeInsets.only(
                left: 10.w,
                right: 10.w,
                bottom: 15.h,
                top: MediaQuery.of(context).padding.top + 8.h,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      if (Get.previousRoute != null)
                        IconButton(
                          icon: Icon(Icons.arrow_back),
                          onPressed: () {
                            Get.back();
                          },
                        ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              chatController.dataGroup.value?['group_name'] ??
                                  "",
                              style: TextStyle(
                                color: buttonLogin,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 50),
                      if (dataExist &&
                          chatController.user != null &&
                          chatController.selectedChats.length == 0) ...[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Wrap(
                              spacing: 10,
                              children: [
                                if (isOwner)
                                  GestureDetector(
                                    onTap: () {
                                      showDialog(
                                          context: context,
                                          builder: (context) {
                                            return REQUEST(
                                              groupId: chatController
                                                  .dataGroup.value!['id'],
                                            );
                                          });
                                    },
                                    child: Icon(Icons.account_box,
                                        color: Color.fromARGB(255, 0, 0, 0)),
                                  ),
                                if (isOwner)
                                  GestureDetector(
                                    onTap: () {
                                      Get.defaultDialog(
                                        titlePadding: EdgeInsets.all(20),
                                        title: "Pengaturan",
                                        content: Container(
                                          child: Column(
                                            children: [
                                              CustomeFilledButton(
                                                width: Get.width / 2,
                                                title: "Edit Topik",
                                                isRounded: false,
                                                color: Colors.amber,
                                                icon: Icon(
                                                  Icons.edit,
                                                  color: whiteColor,
                                                  size: 20,
                                                ),
                                                onPressed: () {
                                                  Get.back();
                                                  TopikService.goToEditTopik(
                                                    chatController
                                                        .dataGroup.value!,
                                                  );
                                                },
                                              ),
                                              const SizedBox(height: 10),
                                              CustomeFilledButton(
                                                width: Get.width / 2,
                                                title: "Hapus Topik",
                                                color: Colors.redAccent,
                                                icon: Icon(
                                                  Icons.delete,
                                                  color: whiteColor,
                                                  size: 20,
                                                ),
                                                isRounded: false,
                                                onPressed: () {
                                                  Get.back();
                                                  chatController.deleteTopik(
                                                    chatController
                                                        .dataGroup.value!['id'],
                                                  );
                                                },
                                              ),
                                            ],
                                          ),
                                        ),
                                      );
                                    },
                                    child: const Icon(
                                      Icons.more_vert_rounded,
                                    ),
                                  ),
                                if (isJoined == true &&
                                    isRequesting != true &&
                                    isOwner != true)
                                  GestureDetector(
                                      onTap: () {
                                        // chatController.keluarGroup(
                                        //     chatController
                                        //         .dataGroup.value!['id']);
                                      },
                                      child: Chip(
                                        backgroundColor:
                                            Color.fromARGB(255, 255, 94, 94),
                                        label: Text(
                                          "Keluar",
                                          style: whiteTextStyle,
                                        ),
                                      )),
                                if (!isOwner && isRequesting == true)
                                  Text(
                                    "(Menunggu Persetujuan)",
                                    style: TextStyle(
                                      color: Colors.amber,
                                    ),
                                  )
                              ],
                            ),
                          ],
                        ),
                      ],
                      if (chatController.selectedChats.length > 0)
                        Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                chatController.showEditDialog(
                                  chatController.editChat,
                                  chatController.editMessage,
                                );
                              },
                              child: const Icon(Icons.edit),
                            ),
                            SizedBox(width: 8.w),
                            GestureDetector(
                              onTap: () {
                                Get.defaultDialog(
                                    title: "TopikChat",
                                    titleStyle: TextStyle(fontWeight: bold),
                                    content: Column(
                                      children: [
                                        Text(
                                            "Apakah anda yakin\ningin menghapus chat ini?"),
                                        SizedBox(
                                          height: 25,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            GestureDetector(
                                              onTap: () {
                                                chatController.deleteChats();
                                                Get.back();
                                              },
                                              child: Text(
                                                "Ya",
                                                style: TextStyle(
                                                  fontWeight: semibold,
                                                  color: blueBlack,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                Get.back();
                                              },
                                              child: Text(
                                                "Tidak",
                                                style: TextStyle(
                                                  fontWeight: semibold,
                                                  color: blueBlack,
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ));
                                // chatController.deleteChats();
                              },
                              child: const Icon(Icons.delete),
                            ),
                          ],
                        )
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 50.w,
                    ),
                    child: Text(
                      "Deskripsi Topik:\n${chatController.dataGroup.value?['description'] ?? ''}",
                      style: const TextStyle(
                        color: Colors.grey,
                        fontSize: 14,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

class SendButton extends StatelessWidget {
  final TextEditingController controller;
  final Function() onTap;
  const SendButton({
    super.key,
    required this.onTap,
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      height: 70.0,
      color: lightBackgroundColor,
      child: Row(
        children: [
          Expanded(
            child: TextField(
              controller: controller,
              decoration: InputDecoration(
                hintText: "Kirim Pesan di Sini.....",
                hintStyle: TextStyle(
                  fontSize: 16,
                  fontWeight: medium,
                  color: greyColor,
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: greenColor),
                  borderRadius: BorderRadius.circular(20),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
            ),
          ),
          IconButton(
            onPressed: onTap,
            icon: Icon(
              Icons.send,
              color: buttonLogin,
            ),
          ),
        ],
      ),
    );
  }
}
