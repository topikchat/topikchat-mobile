import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:topik_chat/app/modules/chat/controllers/chat_controller.dart';
import 'package:topik_chat/shared/theme.dart';
import 'package:topik_chat/app/modules/chat/widget/myChat.dart';

import 'package:flutter/material.dart';
import 'package:topik_chat/shared/theme.dart';

class MyChat extends StatelessWidget {
  final int id;
  final String name;
  final String message;
  final VoidCallback setSelected;
  bool selected;

  MyChat({
    required this.id,
    required this.name,
    required this.message,
    required this.setSelected,
    this.selected = false,
    Key? key,
  }) : super(key: key);

  final ChatController chatController = Get.find<ChatController>();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onLongPress: setSelected,
      child: Container(
        width: Get.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            SizedBox(
              height: 20,
            ),
            Container(
              margin: const EdgeInsets.only(
                right: 40,
                top: 5,
              ),
              child: ClipRRect(
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(20),
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20),
                ),
                child: Stack(
                  children: [
                    Container(
                      color: chatColor,
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Text(
                          message,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
                    ),
                    if (selected) // Add overlay only when selected
                      Positioned.fill(
                        child: Container(
                          color: Colors.blue.withOpacity(0.6),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
