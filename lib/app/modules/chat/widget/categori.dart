import 'package:flutter/material.dart';
import 'package:topik_chat/shared/theme.dart';

class User extends StatefulWidget {
  const User({super.key});

  @override
  State<User> createState() => _UserState();
}

class _UserState extends State<User> {
  int selectedIndex = 0;
  final List<String> categori = ['Messages', 'Group', 'Request'];
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 90.0,
      color: blueColor,
      child: ListView.builder(
          itemCount: categori.length,
          itemBuilder: (BuildContext context, int index) {
            return Text(categori[index]);
          }),
    );
  }
}
