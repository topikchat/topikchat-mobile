import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:topik_chat/app/modules/auth/controllers/auth_controller.dart';
import 'package:topik_chat/app/modules/beranda/views/beranda.dart';
import 'package:topik_chat/app/modules/tambah_topik/views/tambah_topik_view.dart';
import 'package:topik_chat/app/routes/app_pages.dart';
import 'package:topik_chat/shared/dialogs.dart';
import 'package:topik_chat/shared/http_request.dart';
import 'package:topik_chat/shared/theme.dart';

abstract class TopikService {
  static AuthController authController = Get.find<AuthController>();

  static void goToTambahTopik() {
    IconThemeData(
      color: blackColor,
    );
    Get.toNamed(Routes.TAMBAH_TOPIK);
  }

  static void goToEditTopik(topik) {
    Get.toNamed(Routes.TAMBAH_TOPIK, arguments: {"topik": topik});
  }

  static Future<Map<String, dynamic>> tambahTopikProcess({
    String groupName = "",
    String desc = "",
    int isPrivate = 0,
  }) async {
    Dio dio = HttpRequest.dio;
    final GetStorage storage = GetStorage();
    Map<String, dynamic> user = storage.read("user");
    print(jsonEncode(
      {
        'user_id': user['id'],
        'group_name': groupName,
        'description': desc,
        'is_private': isPrivate,
      },
    ));
    try {
      var response = await dio.post(
        "/v1/group-chat",
        data: jsonEncode(
          {
            'user_id': user['id'],
            'group_name': groupName,
            'description': desc,
            'is_private': isPrivate,
          },
        ),
      );
      var data = response.data;
      if (response.statusCode == 201) {
        return data['data'];
      }
      throw Exception(data['message']);
    } on DioError catch (e) {
      throw e.response!.data['message'];
    }
  }

  static Future<String> editTopikProcess({
    int id = 0,
    String groupName = "",
    String desc = "",
    int isPrivate = 0,
  }) async {
    Dio dio = HttpRequest.dio;
    final GetStorage storage = GetStorage();
    Map<String, dynamic> user = storage.read("user");
    try {
      var response = await dio.put("/v1/group-chat/$id",
          data: jsonEncode({
            'group_name': groupName,
            'description': desc,
            'is_private': isPrivate,
          }));

      var data = response.data;
      if (response.statusCode == 200) {
        return data['message'];
      }
      throw Exception(data['message']);
    } on DioError catch (e) {
      throw e.response!.data['message'];
    }
  }
}
