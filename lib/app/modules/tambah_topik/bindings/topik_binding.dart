import 'package:get/get.dart';

import '../controllers/topik_controller.dart';

class TambahTopikBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TopikController>(
      () => TopikController(),
    );
  }
}
