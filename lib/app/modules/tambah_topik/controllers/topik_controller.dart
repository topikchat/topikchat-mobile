import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:topik_chat/app/modules/chat/Service/chat_service.dart';
import 'package:topik_chat/app/modules/chat/views/chat.dart';
import 'package:topik_chat/app/modules/tambah_topik/service/topikService.dart';
import 'package:topik_chat/app/routes/app_pages.dart';
import 'package:topik_chat/shared/dialogs.dart';

class TopikController extends GetxController {
  late final TextEditingController groupNameController;
  late final TextEditingController descController;
  late final TextEditingController isPrivateController;

  @override
  void onInit() {
    groupNameController = TextEditingController();
    descController = TextEditingController();
    isPrivateController = TextEditingController();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    groupNameController.dispose();
    descController.dispose();
    isPrivateController.dispose();
    super.onClose();
  }

  void tambahTopik({
    String groupName = "",
    String desc = "",
    int isPrivate = 0,
  }) async {
    if (groupName == "" || desc == "") {
      dialogError("Kolom judul topik dan deskripsi harus diisi!");
    } else {
      try {
        var data = await TopikService.tambahTopikProcess(
          groupName: groupName,
          desc: desc,
          isPrivate: isPrivate,
        );
        Get.defaultDialog(title: "", middleText: "Berhasil membuat topik baru");
        Future.delayed(
          const Duration(seconds: 1),
          () {
            Get.offAllNamed(Routes.BERANDA);
            Get.offNamed(Routes.CHAT, arguments: {"id": data['id']});
          },
        );
      } catch (e) {
        dialogError(e.toString());
      }
    }
  }

  void editTopik({
    int id = 0,
    String groupName = "",
    String desc = "",
    int isPrivate = 0,
  }) async {
    if (groupName == "" || desc == "") {
      dialogError("Kolom judul topik dan deskripsi harus diisi!");
    } else {
      try {
        var message = await TopikService.editTopikProcess(
          id: id,
          groupName: groupName,
          desc: desc,
          isPrivate: isPrivate,
        );
        Get.defaultDialog(title: "Berhasil", middleText: message);
        Future.delayed(
          const Duration(seconds: 1),
          () {
            Get.offAllNamed(Routes.BERANDA);
            Get.toNamed(Routes.CHAT, arguments: {"id": id});
          },
        );
      } catch (e) {
        dialogError(e.toString());
      }
    }
  }
}
