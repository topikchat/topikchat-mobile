import 'package:flutter/material.dart';
import 'package:topik_chat/shared/theme.dart';

class CustomDropdown extends StatefulWidget {
  final List<String> items;
  final int selectedItem;
  final ValueChanged<int> onChanged;
  String title;

  CustomDropdown({
    this.title = "",
    required this.items,
    required this.selectedItem,
    required this.onChanged,
  });

  @override
  _CustomDropdownState createState() => _CustomDropdownState();
}

class _CustomDropdownState extends State<CustomDropdown> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (widget.title != "")
          Text(
            widget.title,
            style: blackTextStyle.copyWith(
              fontWeight: semibold,
            ),
          ),
        if (widget.title != "")
          SizedBox(
            height: 8,
          ),
        Container(
          width: double.infinity, // Make it full width
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey), // Add a border
            borderRadius:
                BorderRadius.circular(14), // Adjust border radius as needed
          ),
          child: Row(
            children: [
              Expanded(
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                  child: DropdownButton<int>(
                    isExpanded:
                        true, // Make the dropdown expand to the full width
                    value: widget.selectedItem,
                    onChanged: (int? newValue) {
                      setState(() {
                        widget.onChanged(newValue!);
                      });
                    },
                    items: widget.items.map((String entry) {
                      return DropdownMenuItem<int>(
                        value: widget.items.indexOf(entry),
                        child: Text(entry),
                      );
                    }).toList(),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
