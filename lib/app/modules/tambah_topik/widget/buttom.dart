import 'package:flutter/material.dart';
import 'package:topik_chat/shared/theme.dart';

class CustomeFormJudul extends StatelessWidget {
  final String title;
  final double width;
  final double heigth;
  final bool obscureText;
  final TextEditingController? controller;

  const CustomeFormJudul({
    super.key,
    required this.title,
    this.width = double.infinity,
    this.heigth = 50,
    this.obscureText = false,
    this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: blackTextStyle.copyWith(
            fontWeight: semibold,
          ),
        ),
        SizedBox(
          height: 8,
        ),
        TextFormField(
          obscureText: obscureText,
          controller: controller,
          decoration: InputDecoration(
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(14)),
              contentPadding: EdgeInsets.all(12)),
        ),
      ],
    );
  }
}

class CustomeFormDeskripsi extends StatelessWidget {
  final String title;
  final double width;
  final double heigth;
  final bool obscureText;
  final TextEditingController? controller;

  const CustomeFormDeskripsi({
    super.key,
    this.width = double.infinity,
    this.heigth = 100,
    required this.title,
    this.obscureText = false,
    this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: blackTextStyle.copyWith(
            fontWeight: semibold,
          ),
        ),
        SizedBox(
          height: 8,
        ),
        Container(
          height: 120,
          padding: EdgeInsets.symmetric(horizontal: 8),
          decoration: BoxDecoration(
            border: Border.all(),
            borderRadius: BorderRadius.circular(14),
          ),
          child: TextFormField(
            controller: controller,
            decoration: InputDecoration(
              focusedBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              disabledBorder: InputBorder.none,
              focusedErrorBorder: InputBorder.none,
            ),
          ),
        ),
      ],
    );
  }
}
