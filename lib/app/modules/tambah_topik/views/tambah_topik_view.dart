import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:topik_chat/app/modules/auth/service/auth_service.dart';
import 'package:topik_chat/app/modules/auth/views/register.dart';
import 'package:topik_chat/app/modules/chat/Service/chat_service.dart';
import 'package:topik_chat/app/modules/tambah_topik/widget/buttom.dart';
import 'package:topik_chat/app/modules/tambah_topik/widget/dropdown.dart';
import 'package:topik_chat/shared/theme.dart';
import 'package:topik_chat/app/modules/auth/widget/button.dart';
import 'package:topik_chat/app/modules/auth/widget/forms.dart';

import '../controllers/topik_controller.dart';

class TambahTopik extends StatefulWidget {
  // Map<String, dynamic>? topik;

  TambahTopik({Key? key}) : super(key: key);

  @override
  _TambahTopikState createState() => _TambahTopikState();
}

class _TambahTopikState extends State<TambahTopik> {
  List<String> dropdownItems = ['Terbuka', 'Tertutup'];

  int selectedDropdownItem = 0;

  final TopikController controller = Get.find<TopikController>();
  Map<String, dynamic> arguments = {};
  Map? topik;

  @override
  void initState() {
    if (Get.arguments != null) {
      arguments = Get.arguments;
      if (arguments.containsKey("topik")) {
        topik = arguments['topik'];
        controller.groupNameController.text = topik!['group_name'];
        controller.descController.text = topik!['description'];
        setState(() {
          selectedDropdownItem = topik!['is_private'];
        });
      }
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: blackColor),
        backgroundColor: lightBackgroundColor,
        title: Text(
          "TopikChat",
          style: TextStyle(
            color: buttonLogin,
            fontSize: 29,
            fontWeight: semibold,
          ),
        ),
      ),
      backgroundColor: lightBackgroundColor,
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        children: [
          Container(
            margin: const EdgeInsets.only(top: 180),
            padding: const EdgeInsets.all(22),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: whiteColor,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomeFormJudul(
                  title: 'Judul Topik',
                  controller: controller.groupNameController,
                ),
                SizedBox(
                  height: 16,
                ),
                CustomeFormDeskripsi(
                  title: 'Deskripsi',
                  controller: controller.descController,
                ),
                SizedBox(
                  height: 15,
                ),
                CustomDropdown(
                  title: "Status Topik",
                  items: dropdownItems,
                  selectedItem: selectedDropdownItem,
                  onChanged: (int newValue) {
                    setState(() {
                      selectedDropdownItem = newValue;
                    });
                  },
                ),
                SizedBox(
                  height: 15,
                ),
                CustomeFilledButton(
                  title: '${topik != null ? 'Edit' : 'Tambah'} Topik',
                  onPressed: () {
                    if (topik != null) {
                      controller.editTopik(
                        id: topik!['id'],
                        groupName: controller.groupNameController.text,
                        desc: controller.descController.text,
                        isPrivate: selectedDropdownItem,
                      );
                    } else {
                      controller.tambahTopik(
                        groupName: controller.groupNameController.text,
                        desc: controller.descController.text,
                        isPrivate: selectedDropdownItem,
                      );
                    }
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
