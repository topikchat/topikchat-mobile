import 'dart:convert';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:topik_chat/app/modules/auth/service/auth_service.dart';
import 'package:topik_chat/app/modules/beranda/service/berandaService.dart';
import 'package:topik_chat/shared/dialogs.dart';

class BerandaController extends GetxController {
  RxList dataGroupChat = [].obs;
  RxList filteredList = [].obs;
  RxList dataJoinedChat = [].obs;

  BerandaService berandaService = BerandaService();
  final GetStorage storage = GetStorage();
  late var user;

  Future<void> getDataGroupChat() async {
    try {
      var data = await berandaService.getDataGroupChat();
      dataGroupChat.value = data['data'];

      filteredList.value = data['data'];
    } catch (e) {
      dialogError(e.toString());
    }
  }

  Future<void> getJoinedChat() async {
    try {
      var data = await berandaService.getJoinedChat(user['id']);
      dataJoinedChat.value = data['data'];
    } catch (e) {
      dialogError(e.toString());
    }
  }

  @override
  void onInit() {
    user = storage.read("user");
    super.onInit();
  }

  @override
  void onReady() async {
    super.onReady();
    await getDataGroupChat();
    await getJoinedChat();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
