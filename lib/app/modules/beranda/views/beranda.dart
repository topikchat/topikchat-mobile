import 'package:easy_search_bar/easy_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:topik_chat/app/modules/auth/service/auth_service.dart';
import 'package:topik_chat/app/modules/beranda/controllers/beranda_controller.dart';
import 'package:topik_chat/app/modules/beranda/widget/Chat.dart';
import 'package:topik_chat/app/modules/beranda/widget/ChatBar.dart';
import 'package:topik_chat/app/modules/beranda/widget/sidebar.dart';
import 'package:topik_chat/app/modules/chat/Service/chat_service.dart';
import 'package:topik_chat/app/modules/tambah_topik/service/topikService.dart';
import 'package:topik_chat/app/modules/tambah_topik/views/tambah_topik_view.dart';
import 'package:topik_chat/app/routes/app_pages.dart';
import 'package:topik_chat/shared/theme.dart';

class Beranda extends StatefulWidget {
  const Beranda({Key? key});

  @override
  State<Beranda> createState() {
    return _BerandaState();
  }
}

class _BerandaState extends State<Beranda> {
  final berandaController = Get.find<BerandaController>();
  final GlobalKey<ScaffoldState> _drawerKey = GlobalKey();
  String _tipe = "chat";
  String searchText = "";

  void _openDrawer(String tipe) {
    setState(() {
      _tipe = tipe;
    });
    _drawerKey.currentState?.openEndDrawer();
  }

  void filterDataGroupChat() {
    if (searchText.isEmpty) {
      berandaController.filteredList.value = berandaController.dataGroupChat;
      return;
    }

    final List<dynamic> filteredData =
        berandaController.dataGroupChat.where((topik) {
      final groupName = topik['group_name'].toLowerCase();
      final description = topik['description'].toLowerCase();
      final query = searchText.toLowerCase();

      return groupName.contains(query) || description.contains(query);
    }).toList();

    berandaController.filteredList.value = filteredData;

    print("FilteredList: ${berandaController.filteredList}");
  }

  Future<void> refreshData() async {
    await berandaController.getDataGroupChat();
    await berandaController.getJoinedChat();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: refreshData,
      child: Obx(
        () {
          return Scaffold(
            key: _drawerKey,
            endDrawer: _tipe == "chat"
                ? ChatBar(
                    name: 'chat',
                    data: berandaController.dataJoinedChat,
                    loading: false,
                    profile: 'chat',
                  )
                : SideBar(
                    name: berandaController.user['name'],
                    loading: false,
                    profile: 'profile'),
            appBar: EasySearchBar(
              backgroundColor: lightBackgroundColor,
              title: Text(
                "TopikChat",
                style: TextStyle(
                    color: buttonLogin, fontSize: 29, fontWeight: semibold),
              ),
              onSearch: (value) {
                setState(() {
                  searchText = value;
                  filterDataGroupChat();
                });
              },
              iconTheme: IconThemeData(
                color: buttonLogin,
                size: 25,
              ),
              actions: [
                GestureDetector(
                  onTap: () => _openDrawer("chat"),
                  child: Icon(
                    Icons.chat,
                    color: buttonLogin,
                    size: 25,
                  ),
                ),
                SizedBox(
                  width: 11,
                ),
                Icon(
                  Icons.notifications_none_outlined,
                  color: buttonLogin,
                  size: 25,
                ),
                SizedBox(
                  width: 11,
                ),
                GestureDetector(
                  onTap: () => _openDrawer('akun'),
                  child: Icon(
                    Icons.account_circle_outlined,
                    color: buttonLogin,
                    size: 25,
                  ),
                ),
              ],
            ),
            body: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  children: [
                    Container(
                      child: Text(
                        'Cari Topik Percakapan',
                        style: TextStyle(
                            fontSize: 25, fontWeight: medium, height: 3),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20, bottom: 20),
                      child: TextField(
                        decoration: InputDecoration(
                          labelText: 'Game, Makanan, Tugas, dll',
                          prefixIcon: Icon(Icons.search),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        onChanged: (query) {
                          setState(() {
                            searchText = query;
                            filterDataGroupChat();
                          });
                        },
                      ),
                    ),
                    Text(
                      'Rekomendasi Topik',
                      style: TextStyle(
                        fontSize: 25,
                        fontWeight: medium,
                      ),
                    ),
                    buildSearchResult(),
                    SizedBox(
                      height: 30,
                    ),
                    berandaController.filteredList.isEmpty
                        ? Center(
                            child: Column(
                              children: [
                                Icon(Icons.disabled_visible_outlined),
                                SizedBox(
                                  height: 20,
                                ),
                                Text(
                                  'Topik Tidak Ditemukan',
                                  style: TextStyle(
                                      fontSize: 22, fontWeight: regular),
                                ),
                              ],
                            ),
                          )
                        : Column(
                            children: berandaController.dataGroupChat
                                .map(
                                  (topik) => Column(
                                    children: [
                                      TopikCard(
                                        title: topik['group_name'],
                                        Buttontitle: 'Gabung',
                                        onPresed: () {
                                          Get.toNamed(Routes.CHAT,
                                              arguments: {"id": topik['id']});
                                        },
                                        Subtitle: topik['description'],
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                    ],
                                  ),
                                )
                                .toList(),
                          ),
                  ],
                ),
              ),
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                TopikService.goToTambahTopik();
              },
              child: Icon(Icons.add),
            ),
          );
        },
      ),
    );
  }

  Widget buildSearchResult() {
    return Column(
      children: berandaController.filteredList.map((topik) {
        return Column(
          children: [
            TopikCard(
              title: topik['group_name'],
              Buttontitle: 'Gabung',
              onPresed: () {
                Get.toNamed(Routes.CHAT, arguments: {"id": topik['id']});
              },
              Subtitle: topik['description'],
            ),
            SizedBox(height: 15),
          ],
        );
      }).toList(),
    );
  }
}
