// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:topik_chat/app/modules/akun/service/edit_service.dart';
import 'package:topik_chat/app/modules/auth/service/auth_service.dart';
import 'package:topik_chat/shared/theme.dart';

class SideBar extends StatelessWidget {
  SideBar({
    Key? key,
    required this.name,
    required this.loading,
    required this.profile,
  }) : super(key: key);

  final String name;
  final bool loading;
  final String profile;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: lightBackgroundColor,
      child: Container(
        margin: EdgeInsets.only(top: 50),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Container(
                width: 600,
                height: 145,
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.all(10),
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Icon(
                      Icons.account_circle_outlined,
                      size: 84,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      name,
                      style: TextStyle(fontSize: 20, fontWeight: medium),
                    ),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                EditService.editProfile();
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 40, vertical: 15),
                    child: Row(
                      children: [
                        Icon(
                          Icons.account_circle,
                          size: 40,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          'Edit Profile',
                          style: TextStyle(fontSize: 16, fontWeight: medium),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                AuthService.logout();
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 40, vertical: 15),
                    child: GestureDetector(
                      child: Row(
                        children: [
                          Icon(
                            Icons.logout,
                            size: 40,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            'Logout',
                            style: TextStyle(fontSize: 16, fontWeight: medium),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
