import 'package:flutter/material.dart';
import 'package:topik_chat/shared/theme.dart';

class TopikCard extends StatelessWidget {
  final String title;
  final String Buttontitle;
  final String Subtitle;
  final VoidCallback? onPresed;
  const TopikCard({
    super.key,
    required this.title,
    required this.Buttontitle,
    required this.Subtitle,
    required this.onPresed,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 2,
      borderRadius: BorderRadius.circular(20),
      child: Container(
        padding: EdgeInsets.all(30),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: whiteColor,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: TextStyle(fontSize: 16, fontWeight: medium),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              Subtitle,
              style: TextStyle(fontSize: 13, fontWeight: regular),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Icon(
                  Icons.account_circle_outlined,
                ),
                Icon(Icons.account_circle_outlined),
                Icon(Icons.account_circle_outlined)
              ],
            ),
            Row(
              children: [
                Spacer(),
                Container(
                  margin: EdgeInsets.only(),
                  child: SizedBox(
                    width: 90,
                    height: 35,
                    child: TextButton(
                        onPressed: onPresed,
                        style: TextButton.styleFrom(
                            backgroundColor: buttonLogin,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(56),
                            )),
                        child: Text(
                          Buttontitle,
                          style: TextStyle(
                              fontSize: 13,
                              color: lightBackgroundColor,
                              fontWeight: bold),
                        )),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
