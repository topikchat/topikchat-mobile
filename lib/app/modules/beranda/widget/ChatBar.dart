// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:topik_chat/app/modules/chat/Service/chat_service.dart';
import 'package:topik_chat/app/routes/app_pages.dart';
import 'package:topik_chat/shared/theme.dart';

class ChatBar extends StatelessWidget {
  ChatBar({
    Key? key,
    required this.name,
    required this.data,
    required this.loading,
    required this.profile,
  }) : super(key: key);

  final String name;
  final bool loading;
  final String profile;
  final RxList data;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      width: Get.width / 1.3,
      backgroundColor: lightBackgroundColor,
      child: Container(
        margin: EdgeInsets.only(top: 50),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Column(
                children: [
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Container(
                      padding: EdgeInsets.all(10),
                      alignment: Alignment.center,
                      child: Column(
                        children: [
                          Icon(
                            Icons.chat_outlined,
                            size: 80,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Text(
                    "Daftar Perbincanganmu",
                    style: TextStyle(
                      fontSize: 18.sp,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10),
              Divider(),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 35),
                      child: Column(
                        children: data
                            .map(
                              (topik) => Column(
                                children: [
                                  GestureDetector(
                                    onTap: () => Get.toNamed(Routes.CHAT,
                                        arguments: {
                                          'id': topik['group']['id']
                                        }),
                                    child: Container(
                                      padding: const EdgeInsets.only(top: 8.0),
                                      child: Row(
                                        children: [
                                          Icon(
                                            Icons.chat,
                                            size: 40,
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Flexible(
                                            // Wrap text with Flexible
                                            child: Text(
                                              topik['group']['group_name'],
                                              style: TextStyle(
                                                fontSize: 15,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Divider(),
                                  SizedBox(height: 3),
                                ],
                              ),
                            )
                            .toList(),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
