import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:topik_chat/app/modules/auth/controllers/auth_controller.dart';
import 'package:topik_chat/app/modules/beranda/views/beranda.dart';
import 'package:topik_chat/app/modules/tambah_topik/views/tambah_topik_view.dart';
import 'package:topik_chat/shared/http_request.dart';

class BerandaService {
  static AuthController authController = Get.find<AuthController>();
  final GetStorage storage = GetStorage();

  Future<Map<String, dynamic>> getDataGroupChat() async {
    Dio dio = HttpRequest.dio;
    try {
      var response = await dio.get(
        "/v1/group-chat",
      );
      print("getDataGroupChat");

      var data = response.data;

      if (response.statusCode == 200) {
        return data;
      }
      throw Exception(data['message']);
    } on DioError catch (e) {
      throw e.response!.data['message'];
    }
  }

  Future<Map<String, dynamic>> getJoinedChat(int userId) async {
    Dio dio = HttpRequest.dio;
    try {
      var response = await dio.get(
        "/v1/chat-networks?user_id=$userId",
      );
      print("GetjoinedChat");

      var data = response.data;

      if (response.statusCode == 200) {
        return data;
      }
      throw Exception(data['message']);
    } on DioError catch (e) {
      throw e.response!.data['message'];
    }
  }
}
