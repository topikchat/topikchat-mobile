import 'package:get/get.dart';
import 'package:topik_chat/app/modules/auth/service/auth_service.dart';
import '../modules/akun/bindings/akun_binding.dart';
import '../modules/akun/views/akun_view.dart';
import '../modules/akun/views/editProfile.dart';
import '../modules/auth/bindings/auth_binding.dart';
import '../modules/auth/views/login.dart';
import '../modules/auth/views/register.dart';
import '../modules/auth/widget/loadingScreen.dart';
import '../modules/beranda/bindings/beranda_binding.dart';
import '../modules/beranda/views/beranda.dart';
import '../modules/chat/bindings/chat_binding.dart';
import '../modules/chat/views/chat.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/tambah_topik/bindings/topik_binding.dart';
import '../modules/tambah_topik/views/tambah_topik_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.LOADING_SCREEN;

  static final routes = [
    GetPage(
      name: Routes.HOME,
      page: () => const HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: Routes.LOGIN,
      page: () => const LoginScreen(),
      binding: AuthBinding(),
    ),
    GetPage(
      name: Routes.REGISTER,
      page: () => const RegisterScreen(),
      binding: AuthBinding(),
    ),
    GetPage(
      name: Routes.BERANDA,
      page: () => const Beranda(),
      binding: BerandaBinding(),
    ),
    GetPage(
      name: Routes.AKUN,
      page: () => const AkunView(),
      binding: AkunBinding(),
    ),
    GetPage(
      name: Routes.EDITPROFILE,
      page: () => EditProfile(),
      binding: AkunBinding(),
    ),
    GetPage(
      name: Routes.TAMBAH_TOPIK,
      page: () => TambahTopik(),
      binding: TambahTopikBinding(),
    ),
    GetPage(
      name: Routes.LOADING_SCREEN,
      page: () => LoadingScreen(
        toNext: () {
          AuthService.login();
        },
      ),
      binding: AuthBinding(),
    ),
    GetPage(
      name: Routes.CHAT,
      page: () => const BuildChat(),
      binding: ChatBinding(),
    ),
  ];
}
