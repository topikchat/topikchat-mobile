part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = '/home';
  static const REGISTER = '/register';
  static const LOGIN = '/login';
  static const BERANDA = '/beranda';
  static const AKUN = '/akun';
  static const EDITPROFILE = '/editProfile';
  static const TAMBAH_TOPIK = '/tambah-topik';
  static const LOADING_SCREEN = '/loadingScreen';
  static const LOADING_REGISTER = '/loadingRegister';
  static const LOADING_BERANDA = '/loadingBeranda';
  static const CHAT = '/chat';
  static const CHAT_MEMBER = '/chatMember';
}
