import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:get_storage/get_storage.dart';
import 'package:topik_chat/app/modules/auth/service/auth_service.dart';

import '../../env.dart';

class HttpRequest {
  static var options = BaseOptions(
      baseUrl: Env.APIURL,
      headers: {"Accept": "appication/json"},
      followRedirects: false,
      validateStatus: (status) {
        return status! < 500;
      });
  static Dio dio = Dio(options);

  HttpRequest() {
    addInterceptors();
  }

  static void handleToken(token) {
    final GetStorage storage = GetStorage();
    if (token != null) {
      HttpRequest.updateToken(token);
      var user = storage.read("user");
      user['token'] = token;
      storage.write("user", user);
    }
  }

  static void addInterceptors() {
    dio.interceptors.add(InterceptorsWrapper(
      onResponse: (response, handler) {
        var data = response.data["newToken"];
        print("WADUH $data");
        print(response.data);
        handleToken(data);
        return handler.next(response);
      },
      onError: (response, handler) {
        print("$response, thug life");
        if (response.response != null) {
          var data = response.response!.data['newToken'];
          handleToken(data);
          if (response.response!.statusCode == 401) {
            return AuthService.logout(langsung: true);
          }
        }
        return handler.next(response);
      },
    ));
  }

  static void updateToken(String token) {
    options.headers['Authorization'] = "Bearer " + token;
    HttpRequest.dio = Dio(options);
    HttpRequest.addInterceptors();
  }
}
