import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

void dialogError(String msg) {
  Get.defaultDialog(
      title: "Ups ada yang salah",
      middleText: msg,
      radius: 5,
      titleStyle: TextStyle(
          fontFamily: "Poppins", fontSize: 16, fontWeight: FontWeight.bold),
      middleTextStyle: TextStyle(
          fontFamily: "Poppins", fontSize: 14, fontWeight: FontWeight.normal),
      contentPadding: EdgeInsets.symmetric(horizontal: 10));
}

void dialogLoading() {
  Get.defaultDialog(
    title: "Memproses",
    content: const Center(
      child: CircularProgressIndicator(),
    ),
  );
}
